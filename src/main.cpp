// #define DEV
#include "ros/ros.h"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
// #include "opencv2/gpu/gpu.hpp"
#include "sensor_msgs/CompressedImage.h"
// #include <vision.hpp>
using namespace std;
using namespace cv;

// void frame_callback(const sensor_msgs::CompressedImage msg){

//     static Mat frame,frame_scaled;
//     static gpu::GpuMat detected,filtered;
//     static gpu::GpuMat detected_scaled,filtered_scaled;
//     static Mat detected_host,filtered_host;

//     imdecode(msg.data,CV_LOAD_IMAGE_COLOR,&frame);
//     detected.upload(frame);
//     vision::filter(detected);
//     filtered = detected.clone();
//     // cvtColor(detected,filtered,CV_YCrCb2BGR);
//     //vision::detect_object(detected);
//     vision::detect_object(detected);

//     pyrDown(frame,frame_scaled);
//     gpu::pyrDown(filtered,filtered_scaled);
//     gpu::pyrDown(detected,detected_scaled);
//     filtered_scaled.download(filtered_host);
//     detected_scaled.download(detected_host);
//     imshow("Camera",frame_scaled);
//     imshow("Camera_filtered",filtered_host);
//     imshow("Detected",detected_host);
//     waitKey(1);
// }

int main(int argc, char **argv){
    ros::init(argc, argv, "zeabus_vision");
    ros::NodeHandle nh;
    // ros::Subscriber sub = nh.subscribe("image_raw/compressed/",50,frame_callback);

    // vision::init();
    ROS_INFO("Vision initialized.");
    ros::spin();
}