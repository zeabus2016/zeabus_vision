#ifndef __VISON_HPP
#define __VISON_HPP

#include <stdlib.h>
#include "util.hpp"
#include "filter.hpp"
#include "camera_sub.hpp"
#include "obj_detection.hpp"
namespace vision{
    void init(){
        int gpu_count = gpu::getCudaEnabledDeviceCount();
        ROS_INFO("%s",("Nums gpu:" + to_string(gpu_count)).c_str());
    }
}
#endif