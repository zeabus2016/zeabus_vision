/*
author: Teerapath Sujitto (teerapart.s@gmail.com)
Please don't hesitate to contact me if you wish to ask anything.
*/
#ifndef __OBJ_DETECTION_HPP
#define __OBJ_DETECTION_HPP
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
using namespace cv;

namespace vision{

void to_single_col(gpu::GpuMat &input){
    input.reshape(1,input.rows*input.cols);
}

void mergeCols(vector<gpu::GpuMat> &cols,gpu::GpuMat &output){

}

void U8_to_F32_C1(gpu::GpuMat &input,gpu::GpuMat &output){
    input.convertTo(output,CV_32FC1,1/255.0);
}

void detect_edge_gray(gpu::GpuMat &frame){
    const int lowThreshold=60;
    const int ratio=3;
    const int kernel_size=3;
    gpu::GpuMat tmp;
    gpu::blur( frame, tmp, Size(3,3) );
    gpu::Canny( tmp, frame, lowThreshold, lowThreshold*ratio, kernel_size );
}

void detect_edge(gpu::GpuMat &input,gpu::GpuMat &output){
    switch(input.type()){
        case CV_8UC1:
            output = input.clone();
        break;
        case CV_8UC3:
            // cvtColor( input, output, CV_YCrCb2RGB);
            gpu::cvtColor( input, output, CV_BGR2GRAY);
            break;
        default:
            throw "Unsupported mat type for edge detection";
        break;
    }
    detect_edge_gray(output);
}

void kmean(gpu::GpuMat &frame,unsigned char k){
    
}

void kmean(gpu::GpuMat &frame){
    kmean(frame,8);
}

void detect_object(gpu::GpuMat &frame){
    gpu::GpuMat output;
    detect_edge(frame,output);
    kmean(frame);
    frame = output.clone();
}

}//namespace vision
#endif