#ifndef __FILTER_HPP
#define __FILTER_HPP

#include "opencv2/gpu/gpu.hpp"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;

namespace vision{
gpu::FastNonLocalMeansDenoising denoiser;
void filter(gpu::GpuMat &frame){
    try{
        vector<gpu::GpuMat> channels;
        //Convert t YCrCb for histogram equalization
        // gpu::cvtColor(frame,frame,CV_BGR2YCrCb);

        //Equalize only Y channel
        gpu::split(frame,channels);
        gpu::equalizeHist(channels[0],channels[0]);
        gpu::equalizeHist(channels[1],channels[1]);
        gpu::equalizeHist(channels[2],channels[2]);

        gpu::merge(channels,frame);

        //FastNL Denoising for Color;
        //denoiser.labMethod(frame,frame,10,3,8,7);
    }catch(cv::Exception x){
        ROS_INFO("%s",x.msg.c_str());
    }
}

}//namespace vision
#endif