#!/usr/bin/env python3
"NN Training Playground"
import cv2
import numpy as np
import tensorflow as tf
import tensorflowhelper as tfh
import inputpreprocessor.ObjClass2 as ObjClass
import roslib
import rospy
from sensor_msgs.msg import CompressedImage

train_data_height = 512
train_data_width = 640
CAMERA_TOPIC_NAME = "leftcam_top/image_raw/compressed/"
model_name = "model7-a"
padding_size = 0

session = tf.Session()
session_cpu = tf.device("/cpu:0")
def half_sigmoid(input):
    input_bypass, input_to_process = tf.split(3, 2, input)

    input_processed = tf.sigmoid(input_to_process)

    output = tf.concat(3, [input_bypass, input_processed])

    return output

life = tfh.Life(
    tfh.NeuralNetwork(
        layers=[
            tfh.ValidationLayer(shape=[None, train_data_height + padding_size*2, train_data_width + padding_size*2, 3], dtype=tf.uint8),
            tfh.OpLayer(tf.to_float),
            tfh.OpLayer(lambda x: x/255. -0.5),
            tfh.ConvLayer(kernel_width=3, depth_out=10, depth_in=3, padding=True),
            tfh.OpLayer(half_sigmoid),

            tfh.ConvLayer(kernel_width=3, depth_out=10, padding=True),
            tfh.OpLayer(half_sigmoid),
            tfh.ConvLayer(kernel_width=3, depth_out=20, padding=True),
            tfh.OpLayer(half_sigmoid),
            tfh.ConvLayer(kernel_width=3, depth_out=10, padding=True),
            tfh.OpLayer(half_sigmoid),

            tfh.ConvLayer(kernel_width=3, depth_out=2, padding=True),
            tfh.ValidationLayer(shape=[None, train_data_height, train_data_width, 2], dtype=tf.float32),
            # tfh.ReshapeLayer(shape=[None, 2]),
        ]
    ),
    session = session
)
session.run(tf.initialize_all_variables())


def raw_preprocess(img):
    img = cv2.resize(img, (640, 512))
    cv2.cvtColor(img, cv2.COLOR_BGR2YUV, img)

    for index in range(img.shape[2]):
        ch_mean = img[:, :, index].mean()

        if(ch_mean > 128.):
            img[:, :, index] = (img[:, :, index] * (128. / ch_mean)).astype(np.uint8)
        else:
            img[:, :, index] = 255 - img[:, :, index]
            ch_mean = 255 - ch_mean
            img[:, :, index] = (img[:, :, index] * (128. / ch_mean)).astype(np.uint8)
            img[:, :, index] = 255 - img[:, :, index]

    cv2.cvtColor(img, cv2.COLOR_YUV2BGR, img)

    return img

def label_preprocess(label_img):
    label_img = cv2.resize(label_img, (640, 512))
    return label_img

def main():
    """Entry point function"""


    #TD
    # batch_size = 1
    emptyMat = np.zeros((1,train_data_height,train_data_width,3), np.uint8)
    # life.connect_neural_network(sample_input=batch[0], will_train=False)
    life.connect_neural_network(sample_input=emptyMat, will_train=False)

    life.load_saved_model(model_name+"-save-data")
    # life.load_saved_model("auto-save/"+model_name+"-save-data15.0")

    # default weight
    # life.init_var()

    # print(batch[0].dtype, batch[0].shape)
    rospy.init_node('zeabus_vision_convnet')
    sub = rospy.Subscriber(CAMERA_TOPIC_NAME ,CompressedImage, frame_callback,  queue_size = 1)
    # data.get_batch(500)
    rospy.spin()

def frame_callback(ros_data):
    np_arr = np.fromstring(ros_data.data, np.uint8)
    frame = cv2.imdecode(np_arr, 1)
    cv2.imshow("orgiginal_image",frame)
    frame = raw_preprocess(frame)
    data_in = np.array([frame])
    feed_result = life.feed(input_layer_value=data_in)
    hypo = feed_result[0]
    print("HYPO SHAPE", hypo.shape)
    cv2.imshow("image-in", frame)
    cv2.imshow("hypo", ObjClass.combine_label(feed_result[0]))
        # print(feed_result[index])
    cv2.waitKey(30)

if __name__ == "__main__":
    try:
        main()
    except tfh.utilities.TFHError as tfh_error:
        print(tfh_error)
